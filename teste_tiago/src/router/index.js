import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import indexMovies from '@/components/indexMovies'
import createMovies from '@/components/createMovies'
import yearMovies from '@/components/yearMovies'
import showMovies from '@/components/showMovies'
import updateMovies from '@/components/updateMovies'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/movies/index',
      name: 'movies.index',
      component: indexMovies
    },
    {
      path: '/movies/create',
      name: 'movies.create',
      component: createMovies
    },
    {
      path: '/movies/year',
      name: 'movies.year',
      component: yearMovies
    },
    {
      path: '/movies/:id/update/',
      name: 'movies.update',
      component: updateMovies
    },
    {
      path: '/movies/:id',
      name: 'movies.show',
      component: showMovies
    }
  ]
})
